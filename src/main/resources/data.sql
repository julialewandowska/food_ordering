insert into cuisine (cuisine_id, name) values(1, 'POLISH');
insert into cuisine (cuisine_id, name) values(2, 'ITALIAN');
insert into cuisine (cuisine_id, name) values(3, 'MEXICAN');

insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (1, "MAIN_COURSE", "Dumplings", 34.5, 1);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (2, "MAIN_COURSE", "Polish soup", 45.7, 1);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (3, "DESSERT", "Apple pie", 32.4, 1);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (4, "DESSERT", "Doughnut", 23.5, 1);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (5, "MAIN_COURSE", "Pasta", 12.3, 2);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (6, "MAIN_COURSE", "Pizza", 12.3, 2);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (7, "DESSERT", "Tiramisu", 12.3, 2);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (8, "DESSERT", "Panna cotta", 11.00, 2);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (9, "MAIN_COURSE", "Burrito", 10.3, 3);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (10, "MAIN_COURSE", "Quesadilla", 13.4, 3);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (11, "DESSERT", "Churros", 11.2, 3);
insert into lunch (lunch_id, lunch_category, name, price, cuisine_id) values (12, "DESSERT", "Flan", 12.4, 3);

insert into drink (drink_id, name, price) values (1, 'water', 2.2);
insert into drink (drink_id, name, price) values (2, 'juice', 2.3);
insert into drink (drink_id, name, price) values (3, 'pepsi', 4.5);
insert into drink (drink_id, name, price) values (4, 'tea', 1.3);
insert into drink (drink_id, name, price) values (5, 'beer', 2.7);