package com.restaurant.foodordering;

import com.restaurant.foodordering.services.FoodOrderingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class FoodOrderingApplication implements CommandLineRunner {

	@Autowired
	private FoodOrderingService foodOrderingService;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(FoodOrderingApplication.class);
		app.setBannerMode(Banner.Mode.OFF);
		app.run(args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Welcome in our restaurant");
		foodOrderingService.serveCustomer();
	}
}

