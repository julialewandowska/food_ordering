package com.restaurant.foodordering.services;

import com.restaurant.foodordering.models.Lunch;
import com.restaurant.foodordering.models.LunchCategory;
import com.restaurant.foodordering.repositories.LunchRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LunchService {

    private final LunchRepository lunchRepository;

    public LunchService(LunchRepository lunchRepository) {
        this.lunchRepository = lunchRepository;
    }

    public List<Lunch>  findAllByCuisineNameAndCategory(String cuisine, LunchCategory category) {
        return lunchRepository.findByCuisineNameAndLunchCategory(cuisine, category);
    }
}
