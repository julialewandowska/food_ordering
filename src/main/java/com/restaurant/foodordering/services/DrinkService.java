package com.restaurant.foodordering.services;

import com.restaurant.foodordering.models.Drink;
import com.restaurant.foodordering.repositories.DrinkRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrinkService {

    private final DrinkRepository drinkRepository;

    public DrinkService(DrinkRepository drinkRepository) {
        this.drinkRepository = drinkRepository;
    }

    public List<Drink> findAll(){
        return drinkRepository.findAll();
    }
}
