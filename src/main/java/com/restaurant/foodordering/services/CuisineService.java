package com.restaurant.foodordering.services;

import com.restaurant.foodordering.models.Cuisine;
import com.restaurant.foodordering.repositories.CuisineRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CuisineService {

    private final CuisineRepository cuisineRepository;

    public CuisineService(CuisineRepository cuisineRepository) {
        this.cuisineRepository = cuisineRepository;
    }

    public List<Cuisine> findAll(){
        return cuisineRepository.findAll();
    }
}
