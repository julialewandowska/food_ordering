package com.restaurant.foodordering.services;

import com.restaurant.foodordering.models.Cuisine;
import com.restaurant.foodordering.models.Drink;
import com.restaurant.foodordering.models.Lunch;
import com.restaurant.foodordering.models.LunchCategory;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

@Service
public class FoodOrderingService {

    private final Scanner scanner;
    private final CuisineService cuisineService;
    private final LunchService lunchService;
    private final DrinkService drinkService;

    private BigDecimal price = new BigDecimal(0);

    public FoodOrderingService(CuisineService cuisineService, LunchService lunchService, DrinkService drinkService) {
        this.cuisineService = cuisineService;
        this.lunchService = lunchService;
        this.drinkService = drinkService;
        this.scanner = new Scanner(System.in);
    }

    public void serveCustomer(){
        print("Would you like to something to eat?");
        print("1 -> YES \n2 -> NO");
        print("Please enter number 1 if your choice is YES and number 2 if NO");

        int choice = scanner.nextInt();

        while(choice != 1 && choice != 2){
            print("Your choice is wrong, please choice again");
            print("1 -> YES \n2 -> NO");
            choice = scanner.nextInt();
        }

        switch (choice) {
            case 1:
                String cuisine = choseCuisine();
                choseLunch(cuisine);
                orderDrink();
                break;
            case 2:
                orderDrink();
                break;
        }
    }

    private String choseCuisine(){
        List<Cuisine> cuisines = cuisineService.findAll();
        String cuisine = "";
        if(cuisines == null || cuisines.isEmpty()){
            print("We apologize we are out of main courses. Please come tomorrow");
        } else {
            print("Please choose cuisine");

            printCuisines(cuisines);

            int cuisineChoice = scanner.nextInt();

            while(!isChoiceCorrect(cuisines.size(), cuisineChoice)){
                print("Your choice is wrong, please choice again");
                printCuisines(cuisines);
                cuisineChoice = scanner.nextInt();
            }

            for (int i = 0; i < cuisines.size(); i++){
                if(cuisineChoice == i+1){
                    cuisine = cuisines.get(i).getName();
                    break;
                }
            }
            print("You have chosen " + cuisine + " cuisine.");
        }

        return cuisine;
    }

    private void choseLunch(String cuisine){
        String mainCourse = choseMainCourse(cuisine);
        String dessert = choseDessert(cuisine);

        if(!mainCourse.isEmpty() && !dessert.isEmpty()){
            print("You have chosen " + mainCourse + " and " + dessert);
        }
    }

    private String choseMainCourse(String cuisine){
        List<Lunch> mainCourses = lunchService.findAllByCuisineNameAndCategory(cuisine, LunchCategory.MAIN_COURSE);
        String mainCourse = "";
        if(mainCourses == null || mainCourses.isEmpty()){
            print("We apologize we are closing now. We invite you to come tomorrow");
        } else {
            print("Please choose a main course:");
            printLunches(mainCourses);

            int mainCourseChoice = scanner.nextInt();
            while(!isChoiceCorrect(mainCourses.size(), mainCourseChoice)){
                print("Your choice is wrong, please choice again");
                printLunches(mainCourses);
                mainCourseChoice = scanner.nextInt();
            }

            for(int i = 0; i < mainCourses.size(); i++){
                if(mainCourseChoice == i+1){
                    mainCourse = mainCourses.get(i).getName();
                    this.price = this.price.add(new BigDecimal(mainCourses.get(i).getPrice()));
                }
            }
        }

        return mainCourse;
    }

    private String choseDessert(String cuisine){
        List<Lunch> desserts = lunchService.findAllByCuisineNameAndCategory(cuisine, LunchCategory.DESSERT);

        String dessert = "";
        if(desserts == null || desserts.isEmpty()){
            print("We apologize, we are out of desserts. Please come tomorrow");
        } else {
            print("Please choose a main dessert:");
            printLunches(desserts);

            int dessertChoice = scanner.nextInt();
            while (!isChoiceCorrect(desserts.size(), dessertChoice)){
                print("Your choice is wrong, please choice again");
                printLunches(desserts);
                dessertChoice = scanner.nextInt();
            }
            for(int i = 0; i < desserts.size(); i++){
                if(dessertChoice == (i + 1)){
                    dessert = desserts.get(i).getName();
                    this.price = this.price.add(new BigDecimal(desserts.get(i).getPrice()));
                }
            }
        }

        return dessert;
    }

    private String orderDrink(){
        print("Would you like to something to drink?");
        print("1 -> YES \n2 -> NO");

        int choice = scanner.nextInt();

        while(choice != 1 && choice != 2){
            print("Your choice is wrong, please choice again");
            print("1 -> YES \n2 -> NO");
            choice = scanner.nextInt();
        }

        switch(choice) {
            case 1:
                choseDrinks();
                break;
            case 2:
                printPrice();
                break;
            default:
                print("Your choice is wrong, please choice again");
                print("1 -> YES \n2 -> NO");
        }

        return "";
    }

    private void choseDrinks(){
        List<Drink> drinks = drinkService.findAll();
        String drink = "";
        if(drinks == null || drinks.isEmpty()){
            print("We apologize, we are out of drink. Please come tomorrow");
        } else {
            print("Please choose a main dessert:");
            printDrinks(drinks);

            int drinkChoice = scanner.nextInt();
            while (!isChoiceCorrect(drinks.size(), drinkChoice)){
                print("Your choice is wrong, please choice again");
                printDrinks(drinks);
                drinkChoice = scanner.nextInt();
            }
            for(int i = 0; i < drinks.size(); i++){
                if(drinkChoice == (i + 1)){
                    drink = drinks.get(i).getName();
                    this.price = this.price.add(new BigDecimal(drinks.get(i).getPrice()));
                }
            }
        }

        choseLemonOrIce(drink);

        printPrice();
    }

    private void choseLemonOrIce(String drink){
        print("Would you like lemon or ice?");
        print("1 -> Lemon \n2 -> Ice \n3 -> Both \n4 -> None");

        int choice = scanner.nextInt();

        while(!isChoiceCorrect(4, choice)){
            print("Your choice is wrong, please choice again");
            print("1 -> Lemon \n2 -> Ice \n3 -> Both \n4 -> None");

            choice = scanner.nextInt();
        }

        switch (choice) {
            case 1:
                print("I'll bring your " + drink + " with lemon");
                break;
            case 2:
                print("I'll bring your " + drink + " with ice");
                break;
            case 3:
                print("I'll bring your " + drink + " with lemon and ice");
                break;
        }
    }

    private void printCuisines(List<Cuisine> cuisines){
        for (int i = 0; i < cuisines.size(); i++){
            print((i + 1) + " -> " + cuisines.get(i).getName());
        }
    }

    private void printLunches(List<Lunch> lunches){
        for(int i = 0; i < lunches.size(); i++){
            print((i+1) + " -> " + lunches.get(i).getName()
            + " price: " + lunches.get(i).getPrice());
        }
    }

    private void printDrinks(List<Drink> drinks){
        for(int i = 0; i < drinks.size(); i++){
            print((i+1) + " -> " + drinks.get(i).getName()
            + " price: " + drinks.get(i).getPrice());
        }
    }

    private boolean isChoiceCorrect(int numberOfAvailableChoices, int choice){
        List<Integer> choices = new ArrayList<>();
        for(int i = 1; i <= numberOfAvailableChoices; i++){
            choices.add(i);
        }

        return choices.contains(choice);
    }

    private void print(String message){
        System.out.println(message);
    }

    private void printPrice(){
        print("Your bill is: " + this.price.setScale(2, RoundingMode.HALF_UP) + " PLN");
        print("Bye. Hope to see you soon");
    }
}
