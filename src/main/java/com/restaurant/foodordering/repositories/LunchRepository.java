package com.restaurant.foodordering.repositories;

import java.util.List;
import com.restaurant.foodordering.models.Lunch;
import com.restaurant.foodordering.models.LunchCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LunchRepository extends JpaRepository<Lunch, Long>{
  List<Lunch> findByCuisineNameAndLunchCategory(String cuisine, LunchCategory category);
}
