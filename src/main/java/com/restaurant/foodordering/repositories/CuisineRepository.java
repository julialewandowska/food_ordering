package com.restaurant.foodordering.repositories;

import com.restaurant.foodordering.models.Cuisine;
import com.restaurant.foodordering.models.Lunch;
import com.restaurant.foodordering.models.LunchCategory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CuisineRepository extends JpaRepository<Cuisine, Long> {
}
