package com.restaurant.foodordering.repositories;

import com.restaurant.foodordering.models.Drink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DrinkRepository extends JpaRepository<Drink, Long> {
}
