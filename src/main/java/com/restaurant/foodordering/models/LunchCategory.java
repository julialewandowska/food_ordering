package com.restaurant.foodordering.models;

public enum LunchCategory {
    MAIN_COURSE, DESSERT
}
