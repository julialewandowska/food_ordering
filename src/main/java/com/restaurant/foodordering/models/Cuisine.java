package com.restaurant.foodordering.models;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="cuisine")
public class Cuisine {

    @Id
    @Column(name="cuisine_id")
    private  Long id;

    @Column(name="name")
    private String name;

    @OneToMany(mappedBy="cuisine")
    private List<Lunch> items;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Lunch> getItems() {
        return items;
    }

    public void setItems(List<Lunch> items) {
        this.items = items;
    }
}
