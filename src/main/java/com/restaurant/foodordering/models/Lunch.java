package com.restaurant.foodordering.models;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="lunch")
public class Lunch {

    @Id
    @Column(name="lunch_id")
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="lunch_category")
    @Enumerated(EnumType.STRING)
    private LunchCategory lunchCategory;

    @Column(name = "price")
    private Double price;

    @ManyToOne
    @JoinColumn(name="cuisine_id")
    private  Cuisine cuisine;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Cuisine getCuisine() {
        return cuisine;
    }

    public void setCuisine(Cuisine cuisine) {
        this.cuisine = cuisine;
    }

    public LunchCategory getLunchCategory() {
        return lunchCategory;
    }

    public void setLunchCategory(LunchCategory lunchCategory) {
        this.lunchCategory = lunchCategory;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
